import Vue from 'vue'
import VueRouter from 'vue-router'

const Home = () => import('../views/Home.vue')
const Article =() => import('../views/Article.vue')
const Picture =() => import('../views/Picture.vue')
const Movie = () => import('../views/Movie.vue')
const Music = () => import('../views/Music.vue')
const HomeDefault = () => import('../views/HomeDefault.vue')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: '/',
        name: 'home-default',
        component: HomeDefault,
      },
      {
        path: '/article',
        name: 'article',
        component: Article,
      },
      {
        path: '/picture',
        name: 'picture',
        component: Picture
      },
      {
        path: '/movie',
        name: 'movie',
        component: Movie,
      },
      {
        path: '/music',
        name: 'music',
        component: Music,
      }
    ]
  }
]

const router = new VueRouter({
  //mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
